\BOOKMARK [0][-]{chapter*.1}{Resumen}{}% 1
\BOOKMARK [0][-]{chapter*.2}{Abstract}{}% 2
\BOOKMARK [0][-]{chapter*.3}{Agradecimientos}{}% 3
\BOOKMARK [0][-]{chapter*.4}{\315ndice general}{}% 4
\BOOKMARK [0][-]{chapter*.5}{\315ndice de cuadros}{}% 5
\BOOKMARK [0][-]{chapter*.6}{\315ndice de figuras}{}% 6
\BOOKMARK [0][-]{chapter*.7}{\315ndice de listados}{}% 7
\BOOKMARK [0][-]{chapter*.8}{Listado de acr\363nimos}{}% 8
\BOOKMARK [0][-]{chapter.1}{Introducci\363n}{}% 9
\BOOKMARK [1][-]{section.1.1}{T\355tulo del proyecto}{chapter.1}% 10
\BOOKMARK [1][-]{section.1.2}{Estructura del documento}{chapter.1}% 11
\BOOKMARK [1][-]{section.1.3}{Contenido del documento}{chapter.1}% 12
\BOOKMARK [0][-]{chapter.2}{Objetivos}{}% 13
\BOOKMARK [1][-]{section.2.1}{Objetivo general}{chapter.2}% 14
\BOOKMARK [1][-]{section.2.2}{Objetivos espec\355ficos}{chapter.2}% 15
\BOOKMARK [2][-]{subsection.2.2.1}{Captura de eventos}{section.2.2}% 16
\BOOKMARK [2][-]{subsection.2.2.2}{Almacenamiento de eventos}{section.2.2}% 17
\BOOKMARK [2][-]{subsection.2.2.3}{Gestionar los sensores}{section.2.2}% 18
\BOOKMARK [2][-]{subsection.2.2.4}{Presentaci\363n de la informaci\363n}{section.2.2}% 19
\BOOKMARK [2][-]{subsection.2.2.5}{Alertas}{section.2.2}% 20
\BOOKMARK [0][-]{chapter.3}{Antecedentes}{}% 21
\BOOKMARK [1][-]{section.3.1}{Open Data}{chapter.3}% 22
\BOOKMARK [2][-]{subsection.3.1.1}{Beneficios del uso de Open Data}{section.3.1}% 23
\BOOKMARK [2][-]{subsection.3.1.2}{Open Data en Espa\361a}{section.3.1}% 24
\BOOKMARK [1][-]{section.3.2}{Visualizaci\363n de datos}{chapter.3}% 25
\BOOKMARK [2][-]{subsection.3.2.1}{D3 Data Driven Documents}{section.3.2}% 26
\BOOKMARK [2][-]{subsection.3.2.2}{Google Charts Tools}{section.3.2}% 27
\BOOKMARK [2][-]{subsection.3.2.3}{PChart}{section.3.2}% 28
\BOOKMARK [1][-]{section.3.3}{Smart City}{chapter.3}% 29
\BOOKMARK [1][-]{section.3.4}{Contaminaci\363n: calidad del aire, formas de medici\363n y control}{chapter.3}% 30
\BOOKMARK [2][-]{subsection.3.4.1}{Contaminaci\363n atmosf\351rica}{section.3.4}% 31
\BOOKMARK [2][-]{subsection.3.4.2}{Calidad del aire}{section.3.4}% 32
\BOOKMARK [1][-]{section.3.5}{Aplicaci\363n web en detalles}{chapter.3}% 33
\BOOKMARK [1][-]{section.3.6}{Acontecimientos de la actualidad}{chapter.3}% 34
\BOOKMARK [0][-]{chapter.4}{M\351todo de trabajo}{}% 35
\BOOKMARK [1][-]{section.4.1}{Metodolog\355a de trabajo}{chapter.4}% 36
\BOOKMARK [1][-]{section.4.2}{Scrum}{chapter.4}% 37
\BOOKMARK [2][-]{subsection.4.2.1}{Fases de Scrum}{section.4.2}% 38
\BOOKMARK [2][-]{subsection.4.2.2}{Fases Scrum en el proyecto}{section.4.2}% 39
\BOOKMARK [1][-]{section.4.3}{Herramientas}{chapter.4}% 40
\BOOKMARK [2][-]{subsection.4.3.1}{Software utilizado}{section.4.3}% 41
\BOOKMARK [2][-]{subsection.4.3.2}{Hardware utilizado}{section.4.3}% 42
\BOOKMARK [0][-]{chapter.5}{Resultados}{}% 43
\BOOKMARK [1][-]{section.5.1}{ITERACI\323N 0: Recogida de informaci\363n y estado del arte}{chapter.5}% 44
\BOOKMARK [1][-]{section.5.2}{ITERACI\323N 1 y 2: Comunicaci\363n entre la ciudad y el sistema}{chapter.5}% 45
\BOOKMARK [1][-]{section.5.3}{ITERACI\323N 3: Gesti\363n de sensores}{chapter.5}% 46
\BOOKMARK [2][-]{subsection.5.3.1}{A\361adir sensor}{section.5.3}% 47
\BOOKMARK [2][-]{subsection.5.3.2}{Eliminar sensor}{section.5.3}% 48
\BOOKMARK [2][-]{subsection.5.3.3}{Modificar sensor}{section.5.3}% 49
\BOOKMARK [1][-]{section.5.4}{ITERACI\323N 4 y 5: Uso y manejo de informaci\363n}{chapter.5}% 50
\BOOKMARK [2][-]{subsection.5.4.1}{Manejo de la situaci\363n actual}{section.5.4}% 51
\BOOKMARK [2][-]{subsection.5.4.2}{Manejo de datos acontecidos anteriormente}{section.5.4}% 52
\BOOKMARK [2][-]{subsection.5.4.3}{Listado de alarmas por valores excedidos y filtrado}{section.5.4}% 53
\BOOKMARK [1][-]{section.5.5}{ITERACI\323N 6: Interfaz del sistema}{chapter.5}% 54
\BOOKMARK [1][-]{section.5.6}{Despliegue de la aplicaci\363n}{chapter.5}% 55
\BOOKMARK [1][-]{section.5.7}{Valoraci\363n de costes del proyecto}{chapter.5}% 56
\BOOKMARK [0][-]{chapter.6}{Conclusiones}{}% 57
\BOOKMARK [0][-]{appendix.Alph1}{Manual de utilizaci\363n}{}% 58
\BOOKMARK [1][-]{section.Alph1.1}{Men\372 principal}{appendix.Alph1}% 59
\BOOKMARK [1][-]{section.Alph1.2}{A\361adir sensor}{appendix.Alph1}% 60
\BOOKMARK [1][-]{section.Alph1.3}{Modificar sensor}{appendix.Alph1}% 61
\BOOKMARK [1][-]{section.Alph1.4}{Eliminar sensor}{appendix.Alph1}% 62
\BOOKMARK [1][-]{section.Alph1.5}{Visualizaci\363n de informaci\363n}{appendix.Alph1}% 63
\BOOKMARK [2][-]{subsection.Alph1.5.1}{Informaci\363n actual}{section.Alph1.5}% 64
\BOOKMARK [2][-]{subsection.Alph1.5.2}{Informaci\363n semanal}{section.Alph1.5}% 65
\BOOKMARK [2][-]{subsection.Alph1.5.3}{Informaci\363n mensual}{section.Alph1.5}% 66
\BOOKMARK [2][-]{subsection.Alph1.5.4}{Informaci\363n Anual}{section.Alph1.5}% 67
\BOOKMARK [2][-]{subsection.Alph1.5.5}{Alertas por valores excedidos de contaminaci\363n}{section.Alph1.5}% 68
\BOOKMARK [0][-]{appendix*.9}{Referencias}{}% 69
