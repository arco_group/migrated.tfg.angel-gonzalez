\select@language {spanish}
\contentsline {chapter}{Resumen}{\es@scroman {v}}{chapter*.1}
\contentsline {chapter}{Abstract}{\es@scroman {vii}}{chapter*.2}
\contentsline {chapter}{Agradecimientos}{\es@scroman {ix}}{chapter*.3}
\contentsline {chapter}{\'Indice general}{\es@scroman {xiii}}{chapter*.4}
\contentsline {chapter}{\'{I}ndice de cuadros}{\es@scroman {xvii}}{chapter*.5}
\contentsline {chapter}{\'{I}ndice de figuras}{\es@scroman {xix}}{chapter*.6}
\contentsline {chapter}{\IeC {\'I}ndice de listados}{\es@scroman {xxi}}{chapter*.7}
\contentsline {chapter}{Listado de acr\IeC {\'o}nimos}{\es@scroman {xxiii}}{chapter*.8}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}T\IeC {\'\i }tulo del proyecto}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Estructura del documento}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Contenido del documento}{2}{section.1.3}
\contentsline {chapter}{\numberline {2}Objetivos}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Objetivo general}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Objetivos espec\IeC {\'\i }ficos}{5}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Captura de eventos}{5}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Almacenamiento de eventos}{6}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Gestionar los sensores}{6}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Presentaci\IeC {\'o}n de la informaci\IeC {\'o}n}{6}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Alertas}{6}{subsection.2.2.5}
\contentsline {chapter}{\numberline {3}Antecedentes}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Open Data}{7}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Beneficios del uso de Open Data}{9}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Open Data en Espa\IeC {\~n}a}{11}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Visualizaci\IeC {\'o}n de datos}{12}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}D3 Data Driven Documents}{14}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Google Charts Tools}{15}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}PChart}{15}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Smart City}{15}{section.3.3}
\contentsline {section}{\numberline {3.4}Contaminaci\IeC {\'o}n: calidad del aire, formas de medici\IeC {\'o}n y control}{18}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Contaminaci\IeC {\'o}n atmosf\IeC {\'e}rica}{18}{subsection.3.4.1}
\contentsline {subsubsection}{Part\IeC {\'\i }culas}{18}{subsection.3.4.1}
\contentsline {subsubsection}{Gases}{19}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Calidad del aire}{20}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Aplicaci\IeC {\'o}n web en detalles}{22}{section.3.5}
\contentsline {section}{\numberline {3.6}Acontecimientos de la actualidad}{26}{section.3.6}
\contentsline {chapter}{\numberline {4}M\IeC {\'e}todo de trabajo}{27}{chapter.4}
\contentsline {section}{\numberline {4.1}Metodolog\IeC {\'\i }a de trabajo}{27}{section.4.1}
\contentsline {section}{\numberline {4.2}Scrum}{28}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Fases de Scrum}{28}{subsection.4.2.1}
\contentsline {subsubsection}{Pre-game:}{28}{subsection.4.2.1}
\contentsline {subsubsection}{Development:}{29}{subsection.4.2.1}
\contentsline {subsubsection}{Post-game:}{29}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Fases Scrum en el proyecto}{29}{subsection.4.2.2}
\contentsline {subsubsection}{Pre-game}{30}{subsection.4.2.2}
\contentsline {subsubsection}{Development}{31}{subsection.4.2.2}
\contentsline {subsubsection}{Post-Game}{33}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Herramientas}{33}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Software utilizado}{33}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Hardware utilizado}{34}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}Resultados}{35}{chapter.5}
\contentsline {section}{\numberline {5.1}ITERACI\IeC {\'O}N 0: Recogida de informaci\IeC {\'o}n y estado del arte}{35}{section.5.1}
\contentsline {section}{\numberline {5.2}ITERACI\IeC {\'O}N 1 y 2: Comunicaci\IeC {\'o}n entre la ciudad y el sistema}{36}{section.5.2}
\contentsline {section}{\numberline {5.3}ITERACI\IeC {\'O}N 3: Gesti\IeC {\'o}n de sensores}{37}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}A\IeC {\~n}adir sensor}{39}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Eliminar sensor}{40}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Modificar sensor}{41}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}ITERACI\IeC {\'O}N 4 y 5: Uso y manejo de informaci\IeC {\'o}n}{42}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Manejo de la situaci\IeC {\'o}n actual}{42}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Manejo de datos acontecidos anteriormente\newline }{44}{subsection.5.4.2}
\contentsline {subsubsection}{Visualizaci\IeC {\'o}n semanal}{44}{subsection.5.4.2}
\contentsline {subsubsection}{Acontecimientos del \IeC {\'u}ltimo mes}{45}{subsection.5.4.2}
\contentsline {subsubsection}{Estad\IeC {\'\i }stica anual}{46}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Listado de alarmas por valores excedidos y filtrado}{47}{subsection.5.4.3}
\contentsline {section}{\numberline {5.5}ITERACI\IeC {\'O}N 6: Interfaz del sistema}{48}{section.5.5}
\contentsline {section}{\numberline {5.6}Despliegue de la aplicaci\IeC {\'o}n}{49}{section.5.6}
\contentsline {section}{\numberline {5.7}Valoraci\IeC {\'o}n de costes del proyecto\newline }{50}{section.5.7}
\contentsline {chapter}{\numberline {6}Conclusiones}{55}{chapter.6}
\contentsline {chapter}{\numberline {A}Manual de utilizaci\IeC {\'o}n}{59}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}Men\IeC {\'u} principal}{59}{section.Alph1.1}
\contentsline {section}{\numberline {A.2}A\IeC {\~n}adir sensor}{59}{section.Alph1.2}
\contentsline {section}{\numberline {A.3}Modificar sensor}{60}{section.Alph1.3}
\contentsline {section}{\numberline {A.4}Eliminar sensor}{60}{section.Alph1.4}
\contentsline {section}{\numberline {A.5}Visualizaci\IeC {\'o}n de informaci\IeC {\'o}n\newline }{60}{section.Alph1.5}
\contentsline {subsection}{\numberline {A.5.1}Informaci\IeC {\'o}n actual}{60}{subsection.Alph1.5.1}
\contentsline {subsection}{\numberline {A.5.2}Informaci\IeC {\'o}n semanal}{60}{subsection.Alph1.5.2}
\contentsline {subsection}{\numberline {A.5.3}Informaci\IeC {\'o}n mensual}{61}{subsection.Alph1.5.3}
\contentsline {subsection}{\numberline {A.5.4}Informaci\IeC {\'o}n Anual}{61}{subsection.Alph1.5.4}
\contentsline {subsection}{\numberline {A.5.5}Alertas por valores excedidos de contaminaci\IeC {\'o}n}{61}{subsection.Alph1.5.5}
\contentsline {chapter}{Referencias}{63}{appendix*.9}
