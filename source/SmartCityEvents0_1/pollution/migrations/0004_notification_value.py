# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pollution', '0003_notification'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='value',
            field=models.DecimalField(default=0, max_digits=4, decimal_places=1),
        ),
    ]
