# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('sensors', '0002_remove_sensor_sensorid'),
        ('pollution', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pollution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.DecimalField(default=0, max_digits=4, decimal_places=1)),
                ('zone', models.IntegerField(default=0)),
                ('other', models.IntegerField(default=0, null=True)),
                ('other2', models.IntegerField(default=0, null=True)),
                ('edate', models.DateTimeField(default=django.utils.timezone.now)),
                ('sensor', models.ForeignKey(default=None, to='sensors.Sensor')),
            ],
            options={
                'db_table': 'pollution',
            },
        ),
        migrations.DeleteModel(
            name='Polution',
        ),
    ]
