from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import patterns, include, url
from .views import pollutionMainView, pollutionView, graphSensorView, weekGraphView, monthView, monthGraphView, pieGraphSensorView, pieGraphMapView, pieDaysListView, pieGraphView, pollutionNotificationView, pollutionNotificationListView, pollutionNotificationListFilterView

urlpatterns = [
    url(r'^$', pollutionMainView.as_view(), name="pollution_menu"),
    url(r'^no2/$', pollutionView.as_view(), name="pollution_no2"),
    url(r'^so2/$', pollutionView.as_view(), name="pollution_so2"),
    url(r'^co2/$', pollutionView.as_view(), name="pollution_co2"),
    url(r'^o3/$', pollutionView.as_view(),  name="pollution_o3"),
    url(r'^pm25/$', pollutionView.as_view(),  name="pollution_pm25"),
    url(r'^pm10/$', pollutionView.as_view(),  name="pollution_pm10"),
    url(r'^notifications/$', pollutionNotificationView.as_view(), name="notifications"),
    url(r'^notifications/list/$', pollutionNotificationListView.as_view(), name="notificationsList"),
    url(r'^notifications/list/filter/$', pollutionNotificationListFilterView.as_view(), name="notificationsListFIlter"),
    url(r'^map/datapopup/$', graphSensorView.as_view(), name="dpopup"),
    url(r'^week/$', weekGraphView.as_view(), name="graphweek"),
    url(r'^piegraph/$', pieGraphView.as_view(), name="pie"),
    url(r'^piegraph/sensor/$', pieGraphSensorView.as_view(), name="piesensor"),
    url(r'^piegraph/map/$', pieGraphMapView.as_view(), name="piemap"), 
    url(r'^piegraph/daysList/$', pieDaysListView.as_view(), name="piedays"),
    url(r'^month/$', monthView.as_view(), name="month"),
    url(r'^month/graph/$', monthGraphView.as_view(), name="graph_month"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)