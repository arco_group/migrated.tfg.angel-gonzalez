AngularApp = angular.module(
    "AngularApp", [],
    function($interpolateProvider) {
		$interpolateProvider.startSymbol('[[');
		$interpolateProvider.endSymbol(']]');
    }
);