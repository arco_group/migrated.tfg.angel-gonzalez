function alignForm() {
	$(".formModifyNew").height($(".formModifyOld").height());
}

function msg(msg){
	$('#msg').html(msg);
	setTimeout(function(){$('#msg').html("");}, 3000);
}

$('#buttonAdd').on('click', function(){
	$('#msg').css("color", '#8B0000');
	
	if($('#type').val() == ""){
		msg("Type not selected!");
	} 
	else if($('#newlat').val() == ""){
		msg("Latitude error!");
	}
	else if($('#newlon').val() == ""){
		msg("Longitude error!");
	}
	else{
		$('#msg').css("color", '#32CD32');
	    $.post("/management/sensors/add/", $('#form').serialize(), function(data){
	        $('#selector').html(data);
		    $.get("/management/sensors/legendmap/", function(data){
		        $('.legendMap').html(data);
		    });
	    });
	}	
});

$('#buttonRemove').on('click', function(){
	$('#msg').css("color", '#8B0000');

	if($('#selectSensor').val() == ""){
		msg("Sensor not selected!");
	} 
	else{
    	$.post("/management/sensors/remove/", $('#form').serialize(), function(data){
        	$('#selector').html(data);
        	$.get("/management/sensors/legendmap/", function(data){
		        $('.legendMap').html(data);
		    });
    	});
    }
});

$('#buttonModify').on('click', function(){
	$('#msg').css("color", '#8B0000');

	if($('#selectSensor').val() == ""){
		msg("Sensor not selected!");
	} 
	else if($('#newtypecheck').is(":checked") & $('#newtype').val() == ""){
		msg("Type not selected!");
	}
	else if($('#newnamecheck').is(":checked") & $('newname').val() == ""){
		$('newname').val(null);
	}
	else if($('#newlatcheck').is(":checked") & $('#newlat').val() == ""){
		msg("Latitude error!");
	}
	else if($('#newloncheck').is(":checked") & $('#newlon').val() == ""){
		msg("Longitude error!");
	}
	else{
	    $.post("/management/sensors/modify/", $('.form').serialize(), function(data){
	        $('#selector').html(data);
	        $.get("/management/sensors/legendmap/", function(data){
		        $('.legendMap').html(data);
		    });
	    });
	}
});

$(document).ready(function() {

	alignForm();

	$('#newtypecheck').change(function(){
		if($(this).is(":checked")) {
			$('#newtype').removeAttr('disabled');
		}else{
			$('#newtype').attr('disabled','disabled');
		}
	});

	$('#newnamecheck').change(function(){
		if($(this).is(":checked")) {
			$('#newname').removeAttr('disabled');
		}else{
			$('#newname').attr('disabled','disabled');
		}
	});

	$('#newalertcheck').change(function(){
		if($(this).is(":checked")) {
			$('#newalert').removeAttr('disabled');
		}else{
			$('#newalert').attr('disabled','disabled');
		}
	});

	$('#newlatcheck').change(function(){
		if($(this).is(":checked")) {
			$('#newlat').removeAttr('disabled');
		}else{
			$('#newlat').attr('disabled','disabled');
		}
	});

	$('#newloncheck').change(function(){
		if($(this).is(":checked")) {
			$('#newlon').removeAttr('disabled');
		}else{
			$('#newlon').attr('disabled','disabled');
		}
	});

	$('#newdesccheck').change(function(){
		if($(this).is(":checked")) {	
			$('#newdesc').removeAttr('disabled');
		}else{
			$('#newdesc').attr('disabled','disabled');
		}
	});


});