
$(document).ready(function() {
  	$.get("/management/sensors/add/", function(data){
        $('#selector').html(data);
    });

    create_mapsensors_add();

    $.get("/management/sensors/legendmap/", function(data){
        $('.legendMap').html(data);
    });
});

$(document).on('click', '#add', function() { 
    $.get("/management/sensors/add/", function(data){
        $('#selector').html(data);
    });

    create_mapsensors_add();

    $.get("/management/sensors/legendmap/", function(data){
        $('.legendMap').html(data);
    });
});

$(document).on('click', '#modify', function() { 
    $.get("/management/sensors/modify/", function(data){
        $('#selector').html(data);
    });
    
    create_mapsensors_modify();

    $.get("/management/sensors/legendmap/", function(data){
        $('.legendMap').html(data);
    });
});

$(document).on('click', '#remove', function() { 
    $.get("/management/sensors/remove/", function(data){
        $('#selector').html(data);
    });

    create_mapsensors_remove();

    $.get("/management/sensors/legendmap/", function(data){
        $('.legendMap').html(data);
    });
});
