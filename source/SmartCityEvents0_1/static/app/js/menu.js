$(document).ready(function() {
    menu_height();
    main();
});

var height_current = 0;
var contador = 1;
var option = '';

function main () {

	$('.menu_bar').click(function(){
		if (contador == 1) {
			$('nav').animate({
				left: '0'
			});
			contador = 0;
		} else {
			contador = 1;
			$('nav').animate({
				left: '-100%'
			});
		}
	});

};

function close_slide(option) {
	switch(option){
		case '':
			break;
		case 'no2':
			$('.no2').children('.children').slideToggle();
			break;
		case 'so2':
			$('.so2').children('.children').slideToggle();
			break;
		case 'co2':
			$('.co2').children('.children').slideToggle();
			break;
		case 'o3':
			$('.o3').children('.children').slideToggle();
			break;
		case 'pm25':
			$('.pm25').children('.children').slideToggle();
			break;
		case 'pm10':
			$('.pm10').children('.children').slideToggle();
			break;
	}
}

function menu_height(){
    $(".selectmenu").css('height', (parseInt($(".content").css('height')) + parseInt($('.category').css('height'))));
}

function menu_option_click(menu){
    if(option != ''){
		if(option != menu.attr('class')){
			close_slide(option)
			option = menu.attr('class');
		} else{
			option = ''
		}
	} else {
		option = menu.attr('class');
	}
  
	menu.children('.children').slideToggle();	
}





//BUTTONS FOR SLIDED MENU

$(document).on('click', '.no2', function() {
	menu_option_click($(this));	
});

$(document).on('click', '.so2', function() {
	menu_option_click($(this));		
});

$(document).on('click', '.co2', function() {
	menu_option_click($(this));				
});

$(document).on('click', '.o3', function() {
	menu_option_click($(this));			
});

$(document).on('click', '.pm25', function() {
	menu_option_click($(this));				
});

$(document).on('click', '.pm10', function() {
	menu_option_click($(this));				
});





//BUTTONS FOR NO2

$(document).on('click', '#no2', function() {
    $('.category').html("Pollution - NO<sub>2</sub>");
    $('.category').css('display','block');   
    $.get("/pollution/no2/", {'type_pollution': 0}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#no2week', function() { 
    $.get("/pollution/week/", {'type_pollution': 0}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#no2month', function() { 
    $.get("/pollution/month/", {'type_pollution': 0}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#no2year', function() { 
    $.get("/pollution/piegraph/", {'type_pollution': 0}, function(data){
        $('.content').html(data);
        menu_height();
    });
});


$(document).on('click', '#no2alert', function() {
    $.get("/pollution/notifications/", {'type_pollution': 0}, function(data){
        $('.content').html(data);
        menu_height();
    });
});





//BUTTONS FOR SO2

$(document).on('click', '#so2', function() { 
    $('.category').html("Pollution - SO<sub>2</sub>");
    $('.category').css('display','block');    
    $.get("/pollution/so2/", {'type_pollution': 1}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#so2week', function() { 
    $.get("/pollution/week/", {'type_pollution': 1}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#so2month', function() { 
    $.get("/pollution/month/", {'type_pollution': 1}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#so2year', function() { 
    $.get("/pollution/piegraph/", {'type_pollution': 1}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#so2alert', function() { 
    $.get("/pollution/notifications/", {'type_pollution': 1}, function(data){
        $('.content').html(data);
        menu_height();
    });
});





//BUTTONS FOR CO2

$(document).on('click', '#co2', function() {
    $('.category').html("Pollution - CO<sub>2</sub>");
    $('.category').css('display','block');     
    $.get("/pollution/co2/", {'type_pollution': 2}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#co2week', function() { 
    $.get("/pollution/week/", {'type_pollution': 2}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#co2month', function() { 
    $.get("/pollution/month/", {'type_pollution': 2}, function(data){
        $('.content').html(data);
    });
});

$(document).on('click', '#co2year', function() { 
    $.get("/pollution/piegraph/", {'type_pollution': 2}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#co2alert', function() { 
    $.get("/pollution/notifications/", {'type_pollution': 2}, function(data){
        $('.content').html(data);
        menu_height();
    });
});





//BUTTONS FOR O3

$(document).on('click', '#o3', function() {
    $('.category').html("Pollution - O<sub>3</sub>");
    $('.category').css('display','block');       
    $.get("/pollution/o3/", {'type_pollution': 3}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#o3week', function() { 
    $.get("/pollution/week/", {'type_pollution': 3}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#o3month', function() { 
    $.get("/pollution/month/", {'type_pollution': 3}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#o3year', function() { 
    $.get("/pollution/piegraph/", {'type_pollution': 3}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#o3alert', function() { 
    $.get("/pollution/notifications/", {'type_pollution': 3}, function(data){
        $('.content').html(data);
        menu_height();
    });
});





//BUTTONS FOR PM2.5

$(document).on('click', '#pm25', function() {  
    $('.category').html("Pollution - PM2.5</sub>");  
    $('.category').css('display','block');   
    $.get("/pollution/pm25/", {'type_pollution': 4}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#pm25week', function() { 
    $.get("/pollution/week/", {'type_pollution': 4}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#pm25month', function() { 
    $.get("/pollution/month/", {'type_pollution': 4}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#pm25year', function() { 
    $.get("/pollution/piegraph/", {'type_pollution': 4}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#pm25alert', function() { 
    $.get("/pollution/notifications/", {'type_pollution': 4}, function(data){
        $('.content').html(data);
        menu_height();
    });
});




//BUTTONS FOR PM10


$(document).on('click', '#pm10', function() { 
    $('.category').html("Pollution - PM10");
    $('.category').css('display','block');   
    $.get("/pollution/pm10/", {'type_pollution': 5}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#pm10week', function() { 
    $.get("/pollution/week/", {'type_pollution': 5}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#pm10month', function() { 
    $.get("/pollution/month/", {'type_pollution': 5}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#pm10year', function() { 
    $.get("/pollution/piegraph/", {'type_pollution': 5}, function(data){
        $('.content').html(data);
        menu_height();
    });
});

$(document).on('click', '#pm10alert', function() { 
    $.get("/pollution/notifications/", {'type_pollution': 5}, function(data){
        $('.content').html(data);
        menu_height();
    });
});