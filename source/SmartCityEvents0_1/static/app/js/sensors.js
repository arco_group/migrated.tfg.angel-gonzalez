function sensorSelected(){
		
	var attrs = $('#selectSensor').val().split("/");

	if($('#selectSensor').val()==""){
		$('#id').val("");
		$('#type').val("");
		$('#lat').val("");
		$('#lon').val("");
		$('#desc').val("");
	}else{
		$('#id').val(attrs[0]);
		$('#type').val(parseInt(attrs[1]));
		$('#lat').val(parseFloat(attrs[2].replace(',','.').replace(' ','')));
		$('#lon').val(parseFloat(attrs[3].replace(',','.').replace(' ','')));
		$('#desc').val(attrs[4]);
	}
}

function alignForm() {
	$(".formModifyOld").height($(".formModifyNew").height());
}

function msg(type){
	$('#msg').html("Sensor has been successfully "+ type+"!");
	setTimeout(function(){$('#msg').html("");}, 3000);
}

$(document).ready(function() {
	$('#newtypecheck').change(function(){
		if($(this).is(":checked")) {
			$('#newtype').attr('disabled','disabled');
		}else{
			$('#newtype').removeAttr('disabled');
		}
	});

	$('#newlatcheck').change(function(){
		if($(this).is(":checked")) {
			$('#newlat').attr('disabled','disabled');
		}else{
			$('#newlat').removeAttr('disabled');
		}
	});

	$('#newloncheck').change(function(){
		if($(this).is(":checked")) {
			$('#newlon').attr('disabled','disabled');
		}else{
			$('#newlon').removeAttr('disabled');
		}
	});

	$('#newdesccheck').change(function(){
		if($(this).is(":checked")) {
			$('#newdesc').attr('disabled','disabled');
		}else{
			$('#newdesc').removeAttr('disabled');
		}
	});
});