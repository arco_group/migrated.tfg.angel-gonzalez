var clicked = 0;

$(document).ready(function() {	
	load_sensorPie_Empty();
	
	$.get("/pollution/piegraph/map/", {'type_pollution': $('.type_pollution').val()}, function(data){
        $('#mapsensors').html(data);
    });

	$('#piechart').height($('#piesensors'));

});

$(document).on('click', '#piechart', function(e) { /*new RegExp('.*_arc')*/
  	var color = $(e.target).css('fill');
 	
  	if (color == 'rgb(97, 150, 85)'){
  		if (clicked == 1){
  			clicked = 0;
  		}else{
  			clicked = 1
  			load_list(0,25-1); //very low level
  		}
  	}else if (color == 'rgb(150, 166, 61)'){
  		if (clicked == 2){
  			clicked= 0;
  		}else{
  			clicked = 2
  			load_list(25,50-1); //low level
  		}
  	}else if (color == 'rgb(190, 155, 9)'){
  		if (clicked == 3){
  			clicked = 0;
  		}else{
  			clicked = 3
  			load_list(50,75-1); // medium level
  		}
  	}else if (color == 'rgb(204, 79, 57)'){
  		if (clicked == 4){
  			clicked = 0;
  		}else{
  			clicked = 4
  			load_list(75, 100-1); // high level
  		}
  	}else if (color == 'rgb(111, 0, 0)'){
  		if (clicked == 5){
  			clicked = 0;
  		}else{
  			clicked = 5
  			load_list(100, 999); //very high level
  		}
  	}

  	if (clicked == 0 ){
  		$('#days_list').html("<div class='each_day'></br><p id='selectMsg'>(Select pie chart portion to display days)</p></div>");
  		menu_height();
  	}

});

function load_list(level_min, level_max){
	$('#days_list').html("<div class='each_day'></br><p id='selectMsg'>Loading...</p></div>");
	menu_height();
  	$.get("/pollution/piegraph/daysList/", {'level_min': level_min, 'level_max': level_max, 'sensor_id': $('#selectSensor').val()}, function(data){
		$('#days_list').html(data);
		menu_height();
	});
}

function load_sensorPie(){
	$.get("/pollution/piegraph/sensor/", {'sensor_id': $('#selectSensor').val()}, function(data){
		$('#piechart').html(data);
	});
};

function load_sensorPie_Empty(){
	$.get("/pollution/piegraph/sensor/", {'sensor_id': -1}, function(data){
		$('#piechart').html(data);
	});
};


function create_sensorPie(verylow, low, medium, high, veryhigh, sensor_id){
	$('#days_list').html("<div class='each_day'></br><p id='selectMsg'>(Select pie chart portion to display days)</p></div>");
	menu_height(); 
	clicked = 0;

	var title = "Pollution of the year ";
	if (sensor_id > -1) {
		title = title + "(Sensor " + sensor_id + ")";
	}

	var pie = new d3pie("piechart", 	
		{
			"header": {
				"title": {
					"text": title,
					"fontSize": 22,
					"font": "verdana"
				},
				"subtitle": {
					"text": "Number of days that exceeded certain amount of particles (Air Quality Index of EU)",
					"color": "#999999",
					"fontSize": 10,
					"font": "verdana"
				},
				"titleSubtitlePadding": 12
			},
			"size": {
				"canvasWidth": 500,
				"canvasHeight": 400,
				"pieOuterRadius": "90%"
			},
			"data": {
				"content": [
					{
						"label": "0-25",
						"value": verylow,
						"color": "#79bc6a"
					},
					{
						"label": "25-50",
						"value": low,
						"color": "#bbcf4c"
					},
					{
						"label": "50-75",
						"value": medium,
						"color": "#eec20b"
					},
					{
						"label": "75-100",
						"value": high,
						"color": "#FF6347"
					},
					{
						"label": ">100",
						"value": veryhigh,
						"color": "#8B0000"
					}
				]
			},
			"labels": {
				"outer": {
					"pieDistance": 32
				},
				"inner": {
					"format": "value"
				},
				"mainLabel": {
					"fontSize": 11
				},
				"value": {
					"color": "#ffffff",
					"fontSize": 11
				},
				"lines": {
					"enabled": true
				},
				"truncation": {
					"enabled": true
				}
			},
			"effects": {
				"pullOutSegmentOnClick": {
					"effect": "linear",
					"speed": 400,
					"size": 8
				}
			},
			"misc": {
				"gradient": {
					"enabled": true,
					"percentage": 100
				}
			}
		}
	);
};