$(document).ready(function() {  

  $('.checkAll').prop('checked', true);

  d3.tsv(tsv[tsv.length-1], type, function(error, data) {
    if (error) throw error;

    var cities = data.columns.slice(1).map(function(id) {
      return {
        id: id
      };
    });

    // draw legends.
    var legRow = d3.select('.legend').selectAll("div").data(cities).enter()
      .append("span")
        .attr('class', 'each_sensors')
        .style("cursor","pointer")
        .append("div")
          .attr('class', 'each_colors')
          .style("background",function(d,i){ return d3.schemeCategory20[i];})

    d3.select('.legend').selectAll(".each_sensors")
        .append("div")
          .attr('class', 'each_idsensors')
          .append("span")
            .text(function(d){ return d.id;})   

    menu_height(); 

  });

});

$(document).on('click', '.each_idsensors span', function(e) { 
  $('.checkAll').prop('checked', false);

  var sensor = $(e.target).text();
  var i = 0;
  var color = ($(e.target).parent().siblings().css('backgroundColor'));

  d3.selectAll('.city').remove();
  d3.selectAll('g').remove();

  for(i=0;i<7;i++){
    transitionIn(sensor, i, color);
  }
});

$(document).on('click', '.checkAll', function(e) { 
  if ($(this).is(":checked")) {
    $('.checkAll').prop('checked', true);
    var numDay = 0;
    d3.selectAll('g').remove();
    for(numDay=0;numDay<7;numDay++){
      create_graph_week(numDay);
    }

  }else{
     $('.checkAll').prop('checked', true);
  }
});