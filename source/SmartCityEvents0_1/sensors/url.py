from django.conf.urls import patterns, include, url
from .views import addSensorView, removeSensorView, modifySensorView, managementSensorView, mapSensorAddView, mapSensorRemoveView, mapSensorModifyView, mapLegendView

urlpatterns = [
	url(r'^sensors/$', managementSensorView.as_view(), name="management"),
    url(r'^sensors/add/$', addSensorView.as_view(), name="add"),
    url(r'^sensors/remove/$', removeSensorView.as_view(), name="remove"),
    url(r'^sensors/modify/$', modifySensorView.as_view(), name="modify"),
    url(r'^sensors/add/map/$', mapSensorAddView.as_view(), name="sensorsmapadd"),
    url(r'^sensors/remove/map/$', mapSensorRemoveView.as_view(), name="sensorsmapremove"),
    url(r'^sensors/modify/map/$', mapSensorModifyView.as_view(), name="sensorsmapmodify"),
    url(r'^sensors/legendmap/$', mapLegendView.as_view(), name="legendmap"),
]